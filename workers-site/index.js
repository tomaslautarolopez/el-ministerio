import { getAssetFromKV, serveSinglePageApp } from '@cloudflare/kv-asset-handler';

export const servePage = async (event) => {
    try {
        return getAssetFromKV(event, {
            mapRequestToAsset: serveSinglePageApp,
        });
    } catch (e) {
        const pathname = new URL(event.request.url).pathname;
        return new Response(`"${pathname}" not found`, {
            status: 404,
            statusText: 'not found',
        });
    }
};

addEventListener('fetch', (event) => {
    event.respondWith(servePage(event));
});
