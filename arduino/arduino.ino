#include <ArduinoJson.h>

struct Values {
    int knob1;
    int knob2;
    int knob3;
    int knob4;
    int knob5;
    int knob6;
    int knob7;
    int knob8;
    bool button1;
    bool button2;
    bool button3;
    bool button4;
    bool button5;
    bool button6;
};

// for input noise filtering
const int THRESHOLD = 5;

// delay between loop iterations
const int POLLING_DELAY = 15;

struct Values lastValues;
void setup() {
    Serial.begin(115200);

    pinMode(22, INPUT);
    pinMode(24, INPUT);
    pinMode(26, INPUT);
    pinMode(28, INPUT);
    pinMode(30, INPUT);
    pinMode(32, INPUT);

    lastValues.knob1 = 0;
    lastValues.knob2 = 0;
    lastValues.knob3 = 0;
    lastValues.knob4 = 0;
    lastValues.knob5 = 0;
    lastValues.knob6 = 0;
    lastValues.knob7 = 0;
    lastValues.knob8 = 0;

    lastValues.button1 = LOW;
    lastValues.button2 = LOW;
    lastValues.button3 = LOW;
    lastValues.button4 = LOW;
    lastValues.button5 = LOW;
    lastValues.button6 = LOW;
}

void loop() {
    struct Values currentValues;

    currentValues.knob1 = analogRead(A0);
    currentValues.knob2 = analogRead(A1);
    currentValues.knob3 = analogRead(A2);
    currentValues.knob4 = analogRead(A3);
    currentValues.knob5 = analogRead(A4);
    currentValues.knob6 = analogRead(A5);
    currentValues.knob7 = analogRead(A6);
    currentValues.knob8 = analogRead(A7);

    currentValues.button1 = digitalRead(22);
    currentValues.button2 = digitalRead(24);
    currentValues.button3 = digitalRead(26);
    currentValues.button4 = digitalRead(28);
    currentValues.button5 = digitalRead(30);
    currentValues.button6 = digitalRead(32);

    if (somethingChanged(&currentValues)) {
        sendValues(&currentValues);
        lastValues = currentValues;
    }

    delay(POLLING_DELAY);
}

boolean analogValueChanged(int previousValue, int newValue) {
    return abs(newValue - previousValue) > THRESHOLD;
}

boolean somethingChanged(struct Values* currentValues) {
    // analog values
    if (analogValueChanged(lastValues.knob1, currentValues->knob1)) {
        return true;
    }

    if (analogValueChanged(lastValues.knob2, currentValues->knob2)) {
        return true;
    }

    if (analogValueChanged(lastValues.knob3, currentValues->knob3)) {
        return true;
    }

    if (analogValueChanged(lastValues.knob4, currentValues->knob4)) {
        return true;
    }

    if (analogValueChanged(lastValues.knob5, currentValues->knob5)) {
        return true;
    }

    if (analogValueChanged(lastValues.knob6, currentValues->knob6)) {
        return true;
    }

    if (analogValueChanged(lastValues.knob7, currentValues->knob7)) {
        return true;
    }

    if (analogValueChanged(lastValues.knob8, currentValues->knob8)) {
        return true;
    }

    // digital values
    if (lastValues.button1 != currentValues->button1) {
        return true;
    }

    if (lastValues.button2 != currentValues->button2) {
        return true;
    }

    if (lastValues.button3 != currentValues->button3) {
        return true;
    }

    if (lastValues.button4 != currentValues->button4) {
        return true;
    }

    if (lastValues.button5 != currentValues->button5) {
        return true;
    }

    if (lastValues.button6 != currentValues->button6) {
        return true;
    }

    return false;
}

void sendValues(struct Values* currentValues) {
    StaticJsonDocument<256> json;

    json["knob1"] = currentValues->knob1;
    json["knob2"] = currentValues->knob2;
    json["knob3"] = currentValues->knob3;
    json["knob4"] = currentValues->knob4;
    json["knob5"] = currentValues->knob5;
    json["knob6"] = currentValues->knob6;
    json["knob7"] = currentValues->knob7;
    json["knob8"] = currentValues->knob8;

    json["button1"] = currentValues->button1;
    json["button2"] = currentValues->button2;
    json["button3"] = currentValues->button3;
    json["button4"] = currentValues->button4;
    json["button5"] = currentValues->button5;
    json["button6"] = currentValues->button6;

    serializeJson(json, Serial);
}
